from enum import Enum


class CodeGenLanguage(Enum):
    CSharp = 'csharp'
