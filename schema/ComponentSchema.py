from jinja2 import FileSystemLoader, Environment
from schema.FieldsSchema import MemberSchema, TypeSchema
from random import seed
from random import randint


class ComponentSchema:
    def __init__(self, node, generate=True):
        self.node = node
        self.member_schemas = []
        self.include_files = []
        self.custom_implementation = ""

        self.name = self.node['name'].capitalize()

        seed(42069)
        self.id = randint(0, 9999999999)

        members_node = node.get('members', [])
        for member in members_node:
            name, value = member.popitem()
            type_schema = TypeSchema(value)
            member_schema = MemberSchema(type_schema, name)
            self.member_schemas.append(member_schema)

    def render(self, template_path):
        file_loader = FileSystemLoader(template_path)
        env = Environment(loader=file_loader)
        template = env.get_template('component.jinja2')

        output = template.render(component=self)
        return output


    def __str__(self):
        return str(self.node)
