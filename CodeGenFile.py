from typing import List


class CodeGenFile:
    def __init__(self, file_path: str, contents: List[str], stub: bool = False):
        self.file_path = file_path
        self.contents = contents
        self.stub = stub
